#!/bin/python2

""" Fourier Filtering Demo.
    Copyright (C) 2015 Ariel Grunfeld

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
from scipy import signal
from scipy import stats as st
import cv2

NUM = 0
RECORD = False
DELTA = 20

def gaussianKernel(sigma, size=None):
    if size == None:
        size = sigma
    arr = [[(i**2 + j**2)**0.5 for i in range(-size, size+1)] for j in range(-size, size+1)]
    return st.norm.pdf(arr, scale=sigma)

def draw_circle(event, x, y, flags, param):
    xmax = mask.shape[0]
    ymax = mask.shape[1]
    if event == cv2.EVENT_LBUTTONDOWN:
        #cv2.circle(mask, (x,y), 100, (255,255,255), -1)
        #mask[max(0,y-d):min(ymax,y+d),max(0,x-d):min(xmax,x+d)] = True
        mask[y-DELTA:y+DELTA,x-DELTA:x+DELTA] = True
    if event == cv2.EVENT_RBUTTONDOWN:
        #mask[max(0,y-d):min(ymax,y+d),max(0,x-d):min(xmax,x+d)] = False
        mask[y-DELTA:y+DELTA,x-DELTA:x+DELTA] = False

cap = cv2.VideoCapture(0)
fourcc = cv2.cv.CV_FOURCC(*'XVID')
out = cv2.VideoWriter('fourier.avi',fourcc, 20.0, (640,480))

cv2.namedWindow('image', cv2.WINDOW_AUTOSIZE)
cv2.namedWindow('spec', cv2.WINDOW_AUTOSIZE)

cv2.setMouseCallback('spec', draw_circle)

ret, frame = cap.read()
mask = np.zeros((frame.shape[0], frame.shape[1]), dtype = np.bool)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    assert ret

    k = cv2.waitKey(1) & 0xff
    if k == ord('q'):
        break
    elif k == ord('w'):
        NUM = NUM + 1
        print NUM
    elif k == ord('s'):
        NUM = max(0, NUM - 1)
        print NUM
    elif k == ord('e'):
        NUM = NUM + 10
        print NUM
    elif k == ord('d'):
        NUM = max(0, NUM - 10)
        print NUM
    elif k == ord('r'):
        NUM = NUM + 100
        print NUM
    elif k == ord('f'):
        NUM = max(0, NUM - 100)
        print NUM
    elif k == ord('t'):
        NUM = NUM + 1000
        print NUM
    elif k == ord('g'):
        NUM = max(0, NUM - 1000)
        print NUM
    elif k == ord('y'):
        NUM = NUM + 10000
        print NUM
    elif k == ord('h'):
        NUM = max(0, NUM - 10000)
        print NUM
    elif k == ord('x'):
        RECORD = not RECORD
        if (RECORD):
            print "Recording..."
        else:
            print "Stopped Recording."

    # Transform to FreqSpace with fourier
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    v = hsv[:,:,2]
    f = np.fft.fft2(v)

    # Filter
    shape = f.shape
    f = f.ravel()
    f[np.argsort(f)[:NUM]] = 0
    f = np.reshape(f, shape)
    fshift = np.fft.fftshift(f)
    fshift[mask] = 0
    f = np.fft.ifftshift(fshift)

    # Show spectrum
    spec = 20*np.log(np.abs(fshift.real)+1)
    spec = 255 * (spec - np.min(spec)) / (np.max(spec) - np.min(spec) + 1)
    spec = np.uint8(spec)

    # Transform back to AmpSpace
    v = np.fft.ifft2(f)
    v = v.real
    v = np.uint8(v)
    hsv[:,:,2] = v
    frame = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    
    # Display the resulting frame
    frame = cv2.flip(frame, 1)
    cv2.imshow('image',frame)
    cv2.imshow('spec',spec)

    # Record result
    if RECORD:
        out.write(frame)

# When everything done, release the capture
cv2.waitKey(1)
out.release()
cv2.waitKey(1)
cap.release()
cv2.waitKey(1)
#cv2.destroyAllWindows()
cv2.destroyWindow('image')
for i in range(5):
    cv2.waitKey(1)
cv2.destroyWindow('spec')
for i in range(5):
    cv2.waitKey(1)
