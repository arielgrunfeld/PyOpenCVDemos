The scripts here are demonstrations of the OpenCV API for Python 2.

YOU MUST have Python 2 (NOT 3), the OpenCV library, and the API of OpenCV for Python in order to use this code.

Copyright (C) 2015 Ariel Grunfeld
